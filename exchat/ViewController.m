//
//  ViewController.m
//  exchat
//
//  Created by Yuriy Pitomets on 12/25/14.
//  Copyright (c) 2014 Looksery Inc. All rights reserved.
//

#import "ViewController.h"

#import "MessagesLog.h"
#import "Message.h"

#import <CoreData+MagicalRecord.h>


@interface ViewController () <AMBubbleTableDataSource, AMBubbleTableDelegate>

@property (nonatomic, strong) MessagesLog *log;

#pragma mark -

- (void)loadData;
- (Message *)messageByIndexPath:(NSIndexPath *)indexPath;

#pragma mark - chat UI private API fix
- (void)scrollToBottomAnimated:(BOOL)animated;

@end


@implementation ViewController

#pragma mark - Life cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // TODO: make patch for upstream
    self.dataSource = self; // move to super
    self.delegate   = self; // move to super
    self.tableStyle = AMBubbleTableStyleFlat;
    // ???: somewhy required by UI library
//    [self.tableView setContentInset:UIEdgeInsetsMake(64, 0, 0, 0)];
    // customize control
    self.bubbleTableOptions = @{ AMOptionsTimestampEachMessage : @YES
                               , AMOptionsBubblePressEnabled   : @NO
                               , AMOptionsBubbleSwipeEnabled   : @NO
                               };
    // ???: localization
    self.title = NSLocalizedString(@"Messages", @"messages screen title");
    [self loadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // TODO: save & erase self.log
}

#pragma mark - private

- (void)loadData
{
    self.log = [MessagesLog MR_findFirst];

    if (!self.log) {
        self.log = [MessagesLog MR_createEntity];
    }
}

- (Message *)messageByIndexPath:(NSIndexPath *)indexPath
{
    Message *message = self.log.messages[indexPath.row];
    
    if (![message isKindOfClass:[Message class]]) {
        NSAssert(YES, @"Unexpected massage object type.");
    }
    return message;
}

#pragma mark - AMBubbleTableDataSource

- (NSInteger)numberOfRows
{
    return self.log.messages.count;
}

- (AMBubbleCellType)cellTypeForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self messageByIndexPath:indexPath].own ? AMBubbleCellSent
                                                   : AMBubbleCellReceived;
}

- (NSString *)textForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self messageByIndexPath:indexPath].text;
}

- (NSDate *)timestampForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NSDate.date;
}

#pragma mark - AMBubbleTableDelegate

- (void)didSendText:(NSString*)text
{
    Message *message = [Message MR_createEntity];
    message.text = text;
    message.own  = @(YES);
    [self.log addMessagesObject:message];
//    [self.tableView reloadData];
    // ???: somewhy required by UI library
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:(self.log.messages.count - 1) inSection:0];
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
    [self.tableView endUpdates];
    [self scrollToBottomAnimated:YES];
}

@end
