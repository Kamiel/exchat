//
//  Message.h
//  exchat
//
//  Created by Yuriy Pitomets on 12/25/14.
//  Copyright (c) 2014 Looksery Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Message : NSManagedObject

@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSNumber *own;

@end
