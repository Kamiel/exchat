//
//  Message.m
//  exchat
//
//  Created by Yuriy Pitomets on 12/25/14.
//  Copyright (c) 2014 Looksery Inc. All rights reserved.
//

#import "Message.h"


@implementation Message

@dynamic text;
@dynamic own;

@end
