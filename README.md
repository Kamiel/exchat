## ExChat

iOS code challenge App.

### Install

get [CocoaPods](http://cocoapods.org)

```shell
$ git clone --depth=1 https://bitbucket.org/Kamiel/exchat.git
$ cd exchat
$ pod install
```

### Usage

```shell
$ open exchat.xcworkspace
```

Enjoy!
